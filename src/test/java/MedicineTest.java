import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.*;

public class MedicineTest {

    public static MedicineType toMedicineType(String medicineString) {
        return switch (medicineString.toUpperCase()) {
            case "PAINKILLER" -> MedicineType.PAINKILLER;
            case "ANTIBIOTIC" -> MedicineType.ANTIBIOTIC;
            case "VITAMIN" -> MedicineType.VITAMIN;
            case "NULL" -> null;
            default -> throw new IllegalArgumentException("Invalid medicine type: " + medicineString);
        };
    }

    @ParameterizedTest
    @CsvFileSource(resources = "medicine.csv")
    void testMethod1(String type, String days, String dom, String name, String valid) {
        //System.out.println(type + days + dom + name);
        int validDays = Integer.parseInt(days);
        boolean domestic = Boolean.parseBoolean(dom);
        MedicineType medicineType = toMedicineType(type);

        if (valid.equals("invalid")) {
            assertThrows(IllegalArgumentException.class, () -> {new Medicine(name, domestic, medicineType, validDays);});
        }
        else if(valid.equals("valid")) {
            assertDoesNotThrow(() -> {new Medicine(name, domestic, medicineType, validDays);});
        }
        else {
            fail();
        }
    }

    @Test
    public void testValidateCoefficientDomesticAntibiotic() {
        assertEquals(0.1, Medicine.validateCoefficient(true, MedicineType.ANTIBIOTIC, 80));
        assertEquals(1.2, Medicine.validateCoefficient(true, MedicineType.ANTIBIOTIC, 90));
    }

    @Test
    public void testValidateCoefficientDomesticPainkiller() {
        assertEquals(0.3, Medicine.validateCoefficient(true, MedicineType.PAINKILLER, 90));
        assertEquals(1.3, Medicine.validateCoefficient(true, MedicineType.PAINKILLER, 100));
    }

    @Test
    public void testValidateCoefficientDomesticVitamin() {
        assertEquals(0.3, Medicine.validateCoefficient(true, MedicineType.VITAMIN, 100));
        assertEquals(1.1, Medicine.validateCoefficient(true, MedicineType.VITAMIN, 110));
    }

    @Test
    public void testValidateCoefficientNonDomesticAntibiotic() {
        assertEquals(0.2, Medicine.validateCoefficient(false, MedicineType.ANTIBIOTIC, 30));
        assertEquals(2.4, Medicine.validateCoefficient(false, MedicineType.ANTIBIOTIC, 40));
    }

    @Test
    public void testValidateCoefficientNonDomesticPainkiller() {
        assertEquals(0.6, Medicine.validateCoefficient(false, MedicineType.PAINKILLER, 35));
        assertEquals(2.6, Medicine.validateCoefficient(false, MedicineType.PAINKILLER, 40));
    }

    @Test
    public void testValidateCoefficientNonDomesticVitamin() {
        assertEquals(1.6, Medicine.validateCoefficient(false, MedicineType.VITAMIN, 50));
        assertEquals(2.2, Medicine.validateCoefficient(false, MedicineType.VITAMIN, 60));
    }

    @Test
    public void testMedicineCreationValidData() {
        Medicine medicine = new Medicine("Ptamol", true, MedicineType.PAINKILLER, 90);
        assertNotNull(medicine);
        assertEquals("Ptamol", medicine.getMedicineName());
        assertTrue(medicine.getDomestic());
        assertEquals(MedicineType.PAINKILLER, medicine.getMedicineType());
        assertEquals(30.0, medicine.getMedicineCost());
    }
    @Test
    public void testMedicineToString() {
        Medicine medicine = new Medicine("Ptamol", true, MedicineType.PAINKILLER, 90);
        assertEquals("Medicine Name: Ptamol, Cost: 30.0, Type: PAINKILLER", medicine.toString());
    }
}
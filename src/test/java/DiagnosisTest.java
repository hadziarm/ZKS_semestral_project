import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class DiagnosisTest {
    @Nested
    @DisplayName("MedicineType: VITAMIN")
    @Tag("VITAMIN")
    class Vitamin {
        @Test
        public void testDiagnosisCreationValidData() {
            Doctor doctor = new Doctor("Dr. Smith", Specialization.GP, 5, 100000, 25);
            Patient patient = new Patient("John Doe", 35, Issue.FLU, 27.5);
            Medicine medicine = new Medicine("Ptamol", true, MedicineType.VITAMIN, 90);
            LocalDate issued = LocalDate.of(2023, 10, 15);

            Diagnosis diagnosis = new Diagnosis(doctor, patient, medicine, issued);
            assertNotNull(diagnosis);
            assertEquals(doctor, diagnosis.getDoctor());
            assertEquals(patient, diagnosis.getPatient());
            assertEquals(medicine, diagnosis.getMedicine());
            assertEquals(issued, diagnosis.getIssued());
        }

        @Test
        public void testPredictHospitalisationDaysDoctorExperienceHigh() {
            Doctor doctor = new Doctor("Dr. Smith", Specialization.GP, 10, 100000, 25);
            Patient patient = new Patient("John Doe", 35, Issue.FLU, 27.5);
            Medicine medicine = new Medicine("Ptamol", true, MedicineType.VITAMIN, 90);
            LocalDate issued = LocalDate.of(2023, 10, 15);

            Diagnosis diagnosis = new Diagnosis(doctor, patient, medicine, issued);

            // Testing for higher doctor experience leading to lower base days
            int days = diagnosis.predictHospitalisationDays();
            assertTrue(days >= 5 && days <= 20);
        }

        @Test
        public void testPredictHospitalisationDaysMedicineAndIssue() {
            Doctor doctor = new Doctor("Dr. Smith", Specialization.GP, 5, 100000, 25);
            Patient patient = new Patient("John Doe", 35, Issue.HEART_ATTACK, 27.5);
            Medicine medicine = new Medicine("AbX", true, MedicineType.VITAMIN, 90);
            LocalDate issued = LocalDate.of(2023, 10, 15);

            Diagnosis diagnosis = new Diagnosis(doctor, patient, medicine, issued);

            // Testing for specific medicine and patient issue scenario
            int days = diagnosis.predictHospitalisationDays();
            assertEquals(25, days);
        }

        @Test
        public void testPredictHospitalisationDaysAgeVariation() {
            Doctor doctor = new Doctor("Dr. Smith", Specialization.GP, 5, 100000, 25);
            Patient patient = new Patient("John Doe", 60, Issue.HEART_ATTACK, 27.5); // Higher age
            Medicine medicine = new Medicine("Ptamol", true, MedicineType.VITAMIN, 90);
            LocalDate issued = LocalDate.of(2023, 10, 15);

            Diagnosis diagnosis = new Diagnosis(doctor, patient, medicine, issued);

            // Testing for age-based variation
            int days = diagnosis.predictHospitalisationDays();
            assertEquals(30, days);
        }

        @Test
        public void testToString() {
            Doctor doctor = new Doctor("Dr. Smith", Specialization.GP, 5, 100000, 25);
            Patient patient = new Patient("John Doe", 35, Issue.FLU, 27.5);
            Medicine medicine = new Medicine("Ptamol", true, MedicineType.VITAMIN, 90);
            LocalDate issued = LocalDate.of(2023, 10, 15);

            Diagnosis diagnosis = new Diagnosis(doctor, patient, medicine, issued);

            String expectedString = "Diagnosis\n- for> Patient Name: John Doe, Age: 35, Priority Level: 5\n- by dr> Doctor Name: Dr. Smith, Specialization: GP\n- treatment> Medicine Name: Ptamol, Cost: 30.0, Type: VITAMIN\n- issued on> 15.10.2023.";
            assertEquals(expectedString, diagnosis.toString());
        }
    }

    @Nested
    @DisplayName("MedicineType: PAINKILLER")
    @Tag("PAINKILLER")
    class Painkiller {
        @Test
        public void testDiagnosisCreationInvalidData() {
            Doctor doctor = new Doctor("Dr. Smith", Specialization.GP, 5, 100000, 25);
            Patient patient = new Patient("John Doe", 35, Issue.FLU, 27.5);
            Medicine medicine = new Medicine("Ptamol", true, MedicineType.PAINKILLER, 90);
            LocalDate issued = LocalDate.of(2023, 10, 15);

            assertThrows(IllegalArgumentException.class, () -> new Diagnosis(null, patient, medicine, issued));
            assertThrows(IllegalArgumentException.class, () -> new Diagnosis(doctor, null, medicine, issued));
            assertThrows(IllegalArgumentException.class, () -> new Diagnosis(doctor, patient, null, issued));
            assertThrows(IllegalArgumentException.class, () -> new Diagnosis(doctor, patient, medicine, null));

            // Invalid diagnosis scenario
            assertThrows(IllegalArgumentException.class, () -> new Diagnosis(doctor, patient, medicine, issued));
        }

        @Test
        public void testPredictHospitalisationDaysMedicineCostCoefficient() {
            Doctor doctor = new Doctor("Dr. Smith", Specialization.GP, 5, 100000, 25);
            Patient patient = new Patient("John Doe", 35, Issue.BROKEN_BONE, 27.5);
            Medicine medicine = new Medicine("Ptamol", true, MedicineType.PAINKILLER, 90);
            LocalDate issued = LocalDate.of(2023, 10, 15);

            Diagnosis diagnosis = new Diagnosis(doctor, patient, medicine, issued);

            // Testing for different medicine cost coefficients
            int days = diagnosis.predictHospitalisationDays();
            assertEquals(25, days);
        }
    }

    @Nested
    @DisplayName("MedicineType: ANTIBIOTIC")
    @Tag("ANTIBIOTIC")
    class Antibiotic {
        @Test
        public void testDiagnosisCreationInvalidData() {
            Doctor doctor = new Doctor("Dr. Smith", Specialization.GP, 5, 100000, 25);
            Patient patient = new Patient("John Doe", 35, Issue.HEART_ATTACK, 27.5);
            Medicine medicine = new Medicine("Ptamol", true, MedicineType.ANTIBIOTIC, 90);
            LocalDate issued = LocalDate.of(2023, 10, 15);

            assertThrows(IllegalArgumentException.class, () -> new Diagnosis(null, patient, medicine, issued));
            assertThrows(IllegalArgumentException.class, () -> new Diagnosis(doctor, null, medicine, issued));
            assertThrows(IllegalArgumentException.class, () -> new Diagnosis(doctor, patient, null, issued));
            assertThrows(IllegalArgumentException.class, () -> new Diagnosis(doctor, patient, medicine, null));

            // Invalid diagnosis scenario
            assertThrows(IllegalArgumentException.class, () -> new Diagnosis(doctor, patient, medicine, issued));
        }

        @Test
        public void testPredictHospitalisationDaysMedicineCostCoefficient() {
            Doctor doctor = new Doctor("Dr. Smith", Specialization.GP, 5, 100000, 25);
            Patient patient = new Patient("John Doe", 35, Issue.BROKEN_BONE, 27.5);
            Medicine medicine = new Medicine("Ptamol", true, MedicineType.ANTIBIOTIC, 90);
            LocalDate issued = LocalDate.of(2023, 10, 15);

            Diagnosis diagnosis = new Diagnosis(doctor, patient, medicine, issued);

            // Testing for different medicine cost coefficients
            int days = diagnosis.predictHospitalisationDays();
            assertEquals(34, days);
        }
    }


    @Nested
    @DisplayName("MedicineType: mixed")
    @Tag("VITAMIN")
    class Mixed {

        @Test
        public void testGenerateDiagnosisSummaryForPatient() {
            Patient patient = new Patient("John Doe", 35, Issue.BROKEN_BONE, 27.5);

            // Creating sample diagnoses
            Doctor doctor = new Doctor("Dr. Smith", Specialization.GP, 5, 100000, 25);
            Medicine medicine = new Medicine("Ptamol", true, MedicineType.PAINKILLER, 90);
            LocalDate issued = LocalDate.of(2023, 10, 15);
            Diagnosis diagnosis1 = new Diagnosis(doctor, patient, medicine, issued);

            Doctor doctor2 = new Doctor("Dr. Brown", Specialization.GP, 3, 90000, 20);
            Medicine medicine2 = new Medicine("AbX", true, MedicineType.ANTIBIOTIC, 80);
            LocalDate issued2 = LocalDate.of(2024, 5, 20);
            Diagnosis diagnosis2 = new Diagnosis(doctor2, patient, medicine2, issued2);

            // Creating a list of all diagnoses
            List<Diagnosis> allDiagnoses = new ArrayList<>();
            allDiagnoses.add(diagnosis1);
            allDiagnoses.add(diagnosis2);

            // Testing for diagnoses related to a specific patient
            List<Diagnosis> patientDiagnoses = Diagnosis.generateDiagnosisSummary(allDiagnoses, patient);
            assertEquals(2, patientDiagnoses.size());
            assertTrue(patientDiagnoses.contains(diagnosis1));
            assertTrue(patientDiagnoses.contains(diagnosis2));
        }

        @Test
        public void testGenerateDiagnosisSummaryEmptyList() {
            Patient patient = new Patient("Jane Smith", 28, Issue.BROKEN_BONE, 22.0);

            // Creating an empty list of diagnoses
            List<Diagnosis> allDiagnoses = new ArrayList<>();

            // Testing for an empty list result for a patient with no diagnoses
            List<Diagnosis> patientDiagnoses = Diagnosis.generateDiagnosisSummary(allDiagnoses, patient);
            assertTrue(patientDiagnoses.isEmpty());
        }


    }

}

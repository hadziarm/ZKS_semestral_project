public class Patient
{
	private String patientName;
	private int age;
	private int priorityLevel;

	private Issue issue;

	private double bmi;

	public static int priority(int age, Issue issue, double BMI) {
		int priority = 0;
		if(issue == Issue.HEART_ATTACK) {
			if(age > 30){
				if(BMI > 25) {
					priority = 10;
				}
				else {
					priority = 9;
				}
			} else {
				priority = 8;
			}
		}
		else if (issue == Issue.FLU) {
			if(age < 18){
				if(BMI > 18.5) {
					priority = 7;
				}
				else {
					priority = 6;
				}
			} else {
				priority = 5;
			}
		}
		else if (issue == Issue.BROKEN_BONE) {
			if(age < 16){
				priority = 4;
			} else {
				priority = 3;
			}
		}
		return priority;
	}
	public static int priority(Patient patient) {
		return priority(patient.age, patient.issue, patient.bmi);
	}
	public Patient(String patientName, int age, Issue issue, double bmi) {
		if(issue == null) throw new IllegalArgumentException("Issue is null");
		int nameLength = patientName.length();
		if (nameLength < 3 || nameLength > 10) throw new IllegalArgumentException("Patient name length must be between 3 and 10 characters");

		if (age <= 0 || age >= 150) throw new IllegalArgumentException("Bad age");

		if (bmi <= 0 || bmi >= 50) throw new IllegalArgumentException("Bad bmi");

		this.patientName = patientName;
		this.age = age;
		this.priorityLevel = priority(age, issue, bmi);
		this.issue = issue;
		this.bmi = bmi;
	}


	public String getPatientName() {
		return patientName;
	}


	public int getAge() {
		return age;
	}

	public double getBmi() {
		return bmi;
	}

	public Issue getIssue() {
		return issue;
	}

	public int getPriorityLevel() {
		return priorityLevel;
	}

	@Override
	public String toString() {
		return "Patient Name: " + patientName + ", Age: " + age + ", Priority Level: " + priorityLevel;
	}
}
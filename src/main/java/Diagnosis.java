import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Diagnosis {
    private Doctor doctor;
    private Patient patient;

    private Medicine medicine;

    private LocalDate issued;

    private static Random random = new Random();

    private static int generateRandom(int low, int high) {
        return random.nextInt(high - low + 1) + low;
    }

    // Define a format (optional)
    private static DateTimeFormatter formatter =  DateTimeFormatter.ofPattern("dd.MM.yyyy.");;

    public Diagnosis(Doctor doctor, Patient patient, Medicine medicine, LocalDate issued) {
        if (doctor == null) throw new IllegalArgumentException("Bad doctor");
        if (patient == null) throw new IllegalArgumentException("Bad patient");
        if (medicine == null) throw new IllegalArgumentException("Bad medicine");
        if (issued == null) throw new IllegalArgumentException("Bad issued");

        switch (medicine.getMedicineType()) {
            case PAINKILLER -> {
                if(patient.getIssue() == Issue.FLU) {
                    throw new IllegalArgumentException("Bad diagnosis");
                }
            }
            case ANTIBIOTIC -> {
                if(patient.getIssue() == Issue.HEART_ATTACK) {
                    throw new IllegalArgumentException("Bad diagnosis");
                }
            }
        }

        this.doctor = doctor;
        this.patient = patient;
        this.medicine = medicine;
        this.issued = issued;
    }

    public int predictHospitalisationDays() {
        int baseDays;

        // Consider doctor's experience in influencing hospitalization days
        int doctorExperience = doctor.getYearsOfExperience();
        if (doctorExperience >= 10) {
            baseDays = 7;
        } else if (doctorExperience >= 5) {
            baseDays = 15;
        } else {
            baseDays = 35;
        }

        // Add variation based on patient's age
        int ageVariation = patient.getAge() / 5; // Age-based variation
        int totalDays = baseDays + ageVariation;

        // Additional condition based on medicine type and patient issue
        if (medicine.getMedicineType() == MedicineType.ANTIBIOTIC && patient.getIssue() == Issue.HEART_ATTACK) {
            totalDays += 10;
        } else if (medicine.getMedicineType() == MedicineType.PAINKILLER && patient.getIssue() == Issue.FLU) {
            totalDays -= 5;
        }

        // Consider bonus coefficients for medicine costs
        double medicineCostCoefficient = medicine.getMedicineCost() / 100;
        totalDays += (int) (medicineCostCoefficient * 10);

        // Ensure the days are within a certain range
        if (totalDays < 1) {
            totalDays = 1;
        } else if (totalDays > 50) {
            totalDays = 50;
        }

        return totalDays;
    }

    public static List<Diagnosis> generateDiagnosisSummary(List<Diagnosis> allDiagnoses, Patient patient) {
        List<Diagnosis> patientDiagnoses = new ArrayList<>();

        for (Diagnosis diagnosis : allDiagnoses) {
            if (diagnosis.getPatient().equals(patient)) {
                patientDiagnoses.add(diagnosis);
            }
        }

        return patientDiagnoses;
    }

    public static DateTimeFormatter getFormatter() {
        return formatter;
    }

    @Override
    public String toString() {
        return "Diagnosis" +
                "\n- for> " + patient +
                "\n- by dr> " + doctor +
                "\n- treatment> " + medicine +
                "\n- issued on> " + issued.format(formatter);
    }

    public Doctor getDoctor() {
        return doctor;
    }



    public Patient getPatient() {
        return patient;
    }



    public Medicine getMedicine() {
        return medicine;
    }



    public LocalDate getIssued() {
        return issued;
    }


}

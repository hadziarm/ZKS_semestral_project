public class Medicine {
	private String medicineName;
	private double medicineCost;
	private MedicineType medicineType;

	private boolean domestic;

	public static double validateCoefficient(boolean domestic, MedicineType medicineType, int daysValid) {
		double coefficient = 1;

		if (domestic) {
			if (medicineType == MedicineType.ANTIBIOTIC) {
				if (daysValid <= 80){
					coefficient = 0.1;
				}
				else{
					coefficient = 1.2;
				}
			}
			else if (medicineType == MedicineType.PAINKILLER) {
				if (daysValid <= 90){
					coefficient = 0.3;
				}
				else{
					coefficient = 1.3;
				}
			}
			else if (medicineType == MedicineType.VITAMIN) {
				if (daysValid <= 100){
					coefficient = 0.3;
				}
				else{
					coefficient = 1.1;
				}
			}
		}
		else {
			if (medicineType == MedicineType.ANTIBIOTIC) {
				if (daysValid <= 30){
					coefficient = 0.2;
				}
				else{
					coefficient = 2.4;
				}
			}
			else if (medicineType == MedicineType.PAINKILLER) {
				if (daysValid <= 35){
					coefficient = 0.6;
				}
				else{
					coefficient = 2.6;
				}
			}
			else if (medicineType == MedicineType.VITAMIN) {
				if (daysValid <= 50){
					coefficient = 1.6;
				}
				else{
					coefficient = 2.2;
				}
			}
		}

		return coefficient;
	}
	
	public Medicine(String medicineName, boolean domestic, MedicineType medicineType, int daysValid) {
		if (medicineType == null) throw new IllegalArgumentException("Medicine type is null");
		int nameLength = medicineName.length();
		if (nameLength < 3 || nameLength > 10) throw new IllegalArgumentException("Medicine name length must be between 3 and 10 characters");

		if (daysValid <= 0) throw new IllegalArgumentException("Medicine is no longer valid");

		double coefficient = validateCoefficient(domestic, medicineType, daysValid);

		this.medicineName = medicineName;
		this.medicineCost = coefficient * 100;
		this.domestic = domestic;
		this.medicineType = medicineType;

	}

	public String getMedicineName() {
		return medicineName;
	}


	public double getMedicineCost() {
		return medicineCost;
	}

	public boolean getDomestic() {
		return domestic;
	}

	public MedicineType getMedicineType() {
		return medicineType;
	}

	@Override
	public String toString() {
		return "Medicine Name: " + medicineName + ", Cost: " + medicineCost + ", Type: " + medicineType;
	}

}